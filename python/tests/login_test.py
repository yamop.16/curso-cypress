# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import unittest, time, re

class UntitledTestCase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Remote(command_executor='http://127.0.0.1:8910',desired_capabilities=DesiredCapabilities.PHANTOMJS)
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_untitled_test_case(self):
        driver = self.driver
        driver.get("http://exikhan-eie.dyndns.org:8081/")
        driver.find_element_by_id("cd_username-inputEl").click()
        driver.find_element_by_id("cd_username-inputEl").clear()
        driver.find_element_by_id("cd_username-inputEl").send_keys("MIKE")
        driver.find_element_by_id("tx_password-inputEl").click()
        driver.find_element_by_id("tx_password-inputEl").clear()
        driver.find_element_by_id("tx_password-inputEl").send_keys("mpa001")
        driver.find_element_by_id("window-1013-innerCt").click()
        driver.find_element_by_id("btn_siguiente-btnIconEl").click()
        self.assertRegexpMatches(driver.find_element_by_xpath("//div[@id='details-panel-innerCt']/table[2]/tbody/tr/td[2]/font").text, r"^[\s\S]*MIGUEL[\s\S]*$")
        driver.find_element_by_xpath("//div[@id='details-panel-innerCt']/table/tbody/tr/td[2]/a/font").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()